<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaseTable extends Migration
{
    /**
     * Run the migrations.
     *     *
     * @return void
     */
    public function up()
    {
        Schema::create('lease', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pm_id')->unsigned();
            $table->foreign('pm_id')->references('id')->on('pmagreement');
            $table->integer('property_id')->unsigned();
            $table->foreign('property_id')->references('id')->on('propertyaddress');           
            $table->integer('tenant_id')->unsigned();
            $table->foreign('tenant_id')->references('id')->on('tenant');
            $table->integer('agent_id')->unsigned();
            $table->foreign('agent_id')->references('id')->on('agent');
            $table->dateTime('lease_start');
            $table->dateTime('lease_end');
            $table->integer('rent');
            $table->integer('late_fee');
            $table->integer('security_deposit');
            $table->integer('pet_deposit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lease');
    }
}
